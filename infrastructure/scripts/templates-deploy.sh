#!/bin/bash

set -e

AWS_ACCOUNT_ID=${2}

aws s3api head-bucket --bucket "$1" || aws s3 mb "s3://${1}"

aws s3api put-bucket-policy --bucket "$1" \
  --policy "{\"Version\":\"2012-10-17\",\"Statement\":[{\"Effect\":\"Allow\",\"Principal\":{\"AWS\":[\"arn:aws:iam::${AWS_ACCOUNT_ID}:user/dc-ssr-cicd\"]},\"Action\":[\"s3:GetObject\",\"s3:GetObjectVersion\"],\"Resource\":\"arn:aws:s3:::${1}/*\"},{\"Effect\":\"Allow\",\"Principal\":{\"AWS\":[\"arn:aws:iam::${AWS_ACCOUNT_ID}:user/dc-ssr-cicd\"]},\"Action\":[\"s3:ListBucket\",\"s3:GetBucketVersioning\"],\"Resource\":\"arn:aws:s3:::${1}\"}]}"

aws s3api put-bucket-versioning --bucket "$1" --versioning-configuration Status=Enabled

pwd

aws s3 cp infrastructure/ "s3://${1}" --exclude "*" --include "*.yml" --recursive

